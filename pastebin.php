<?php
include("geshi.php");
class Pastebin {
	private $_configfile = "pastebin.conf";

	private $_config = array(
		"header"   => NULL,
		"footer"   => NULL,
		"database" => NULL,
		"devmode"  => NULL,
		"db"       => NULL
	);

	private $_syntax;

	/* __construct {{{ */
	/**
	 * Class constructor that reads configuration and opens a db link
	 */
	public function __construct() {
		/* read config */
		$this->_config = $this->_read_conf($this->_configfile);
		if ($this->_config["devmode"])
			die("This site is down for maintainance. Please come back later");
		$this->_config["db"] = $this->_init_db($this->_config["database"]);
		/* load syntax languages from geshi */
		$this->_syntax = $this->_load_syntax();
	}
	/* }}} */
	/* run {{{ */
	/**
	 * Do the actual work based on parameters
	 *
	 * @param array $params the parameters
	 * @todo document the parameter structure
	 */
	public function run($params = array()) {
		/* find out what to do */
		switch($params["action"]) {
		case "save":
			$paste_id = $this->_store_paste($params["paste"], $params["syntax"]);
			header("Location: ?action=view&id=$paste_id");
			break;
		case "view":
			$paste = $this->_get_paste($params["id"]);
			$this->_render_page($paste);
			break;
		default:
			$this->_render_page();
			break;
		}
	}
	/* }}} */
	/* _render_page {{{ */
	/**
	 * Create html and output it to the client
	 *
	 * @param string $data HTML to show to client
	 */
	private function _render_page($data = "") {
		if (get_magic_quotes_gpc() && $data["pastedata"])
			$data["pastedata"] = stripslashes($data["pastedata"]);
		/* print header */
		if ($this->_config["header"] && file_exists($this->_config["header"]))
			require_once($this->_config["header"]);
		/* print page content. If $data is an array we are viewing a paste.
		   if not, we show the form to create onne */
		if (is_array($data)) {
			/* unset syntax if 'Generic' was selected */
			if ($data["syntax"] == "Generic")
				$data["syntax"] = "";
			$url = "";
			if ($_SERVER["HTTPS"] == "on")
				$url .= "https://";
			else
				$url .= "http://";
			$url .= $_SERVER["HTTP_HOST"];
			$url .= dirname($_SERVER["PHP_SELF"]);
			$url .= "/?action=view&id=".$data["id"];
			echo "<p class=\"pastebinpage\">\n";
			echo "You are looking at pastebin item ".$data["id"]."<br />\n";
			echo "pastebin url: $url";
			$geshi = new Geshi($data["pastedata"], $data["syntax"]);
			$geshi->enable_classes();
			echo "<style type=\"text/css\">\n";
			echo $geshi->get_stylesheet();
			echo "</style>\n";
			$geshi->enable_line_numbers(GESHI_NORMAL_LINE_NUMBERS);
			echo "<div class=\"pastebindata\">\n";
			echo $geshi->parse_code();
			echo "</div>\n";
			echo "</p>";
			echo "<p class=\"pastebinpage\">\n";
			echo "<a href=\"index.php\">pastebin your own stuff</a>\n";
			echo "</p>";
		} else {
			echo "<form id=\"pastebin\" method=\"post\" action=\"index.php\">\n";
			echo "<p class=\"pastebinpage\">\n";
			echo "<input type=\"hidden\" name=\"action\" value=\"save\" />\n";
			echo "<textarea name=\"paste\" rows=\"1\" cols=\"1\" wrap=\"off\" class=\"pastebinarea\"></textarea><br />\n";
			echo "<select name=\"syntax\">\n";
			foreach ($this->_syntax as $v)
				echo "<option name=\"$v\">$v</option>\n";
			echo "</select>\n";
			echo "<input type=\"submit\" value=\"pastebin it\" class=\"submitbutton\" />&nbsp;&nbsp;<input type=\"reset\" class=\"resetbutton\" />\n";
			echo "</p>\n";
			echo "</form>\n";
		}
		/* print footer */
		if ($this->_config["footer"] && file_exists($this->_config["footer"]))
			require_once($this->_config["footer"]);
	}
	/* }}} */
	/* _read_conf {{{ */
	/**
	 * Read configfile into conf array
	 *
	 * @param string $configfile The configuration file
	 * @return array The configname/value
	 */
	private function _read_conf($configfile) {
		/* put config stuff in place */
		$config = array();
		if (!$confdata = file($configfile))
			die("error reading configfile");
		foreach ($confdata as $configline) {
			if (trim($configline) == "" || preg_match("/^#/", $configline))
				continue;
			$confpair = explode("=", $configline);
			$config[trim(str_replace("\"", "", $confpair[0]))] = trim(str_replace("\"", "", $confpair[1]));
		}
		return $config;
	}
	/* }}} */
	/* _init_db {{{ */
	/**
	 * Initiate database connection and create db if it does not exist
	 *
	 * @param string $database The database to use
	 * @return mixed db link resource identifier
	 */
	private function _init_db($database) {
		$_create_tables = false;
		if (preg_match("/^\//", $database))
			die("invalid database");
		/* if the database file is not there, we need to put some tables in there */
		if (!file_exists($database))
			$_create_tables = true;
		if ($db = sqlite_open($database, 0666, $error)) {
			if ($_create_tables)
				sqlite_query($db, "CREATE TABLE pastebin ( id varchar(100), pastedata text, date int(11), syntax varchar(255) )");
			return $db;
		} else {
			die($error);
		}
	}
	/* }}} */
	/* _generate_id {{{ */
	/**
	 * generate a random id and check if it's free
	 *
	 * @return string a random id that is free
	 */
	private function _generate_id() {
		$id = sprintf("%06x", rand());
		$res = sqlite_query($this->_config["db"], sprintf("SELECT * FROM pastebin WHERE id = '%s'", $id));
		if (sqlite_num_rows($res))
			$id = $this->_generate_id();
		else
			return $id;
	}
	/* }}} */
	/* _get_paste {{{ */
	/**
	 * Get a specific post from database
	 *
	 * @param string $id The paste to get
	 * @return array the paste
	 */
	private function _get_paste($id) {
		$sql = sprintf("SELECT * FROM pastebin WHERE id = '%s'",
			$id );
		$res = sqlite_query($this->_config["db"], $sql);
		$data = sqlite_fetch_array($res, SQLITE_ASSOC);
		return $data;

	}
	/* }}} */
	/* _store_paste {{{ */
	/**
	 * save a paste to database
	 *
	 * @param string $paste The paste text
	 * @return string the id of the new paste
	 */
	private function _store_paste($paste, $syntax = "") {
		$id = $this->_generate_id();
		$ts = mktime();
		$paste = sqlite_escape_string($paste);
		$sql = sprintf("INSERT INTO pastebin VALUES ('%s', '%s', %d, '%s');",
			$id, $paste, $ts, $syntax);
		if (!sqlite_query($this->_config["db"], $sql))
			die("error saving post");
		return $id;
	}
	/* }}} */
	/* _load_syntax {{{ */
	/**
	 * Load all the syntax files we uploaded from the geshi project.
	 *
	 * This wont return anything, but will set the class variable _syntax
	 * as array with all the languages we know. Later we use this in the post
	 * form to give the user the dropdown for language of their pastebin
	 *
	 * @param string $path The path where we can find the syntax files
	 */
	private function _load_syntax($path = "geshi/") {
		/* ls dir */
		$dh = opendir($path);
		while (false !== ($file = readdir($dh))) {
			if ($file != "." && $file != ".." && !preg_match("/^\./", $file))
				/* remove .php */
				$_files[] = substr($file,0,strlen($file)-4);
		}
		/* sort */
		natsort($_files);
		$files = array("Generic");
		$files = $files+$_files;
		return $files;
	}
	/* }}} */
}
?>
